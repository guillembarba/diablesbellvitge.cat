## -*- coding: utf-8 -*-
${set_locale(lang)}
<!DOCTYPE html>
<html lang="${lang}">
<head>
    <%block name="html_head">
        <title>${title} | ${blog_title}</title>
        <meta name="description" content="${description}" >
        <meta name="author" content="${blog_author}">

        <!-- META TAGS  -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

        <!-- CSS FILES  -->
        <link href="/assets/css/animate.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/normalize.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css"/>
        <%block name="extra_styles">
        </%block>
        <link href="/assets/css/grid.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/media.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </%block>
</head>
%if isinstance(permalink, tuple):
<body id="home">
%else:
<body>
%endif
    <!--<div id="loading"></div>-->
    <!--<div id="curtain"></div>-->
    <!-- RIGHT-LEFT ARROWS  -->
    <!--<div id="arrows">
        <a id="next"></a>
        <a id="prev"></a>
    </div>-->
    <!-- TOP MENU ITEMS  -->
    %for id, url, reveal_id, text in top_menu_links[lang]:
        %if url:
            <a id="${id}" href="${url}" class="menuitem">
                <span class="info">${text}</span>
            </a>
        %elif reveal_id:
            <a id="${id}" class="menuitem" data-reveal-id="${reveal_id}">
                <span class="info">${text}</span>
            </a>
        %else:
            <a id="${id}" class="menuitem">
                <span class="info">${text}</span>
            </a>
        %endif
    %endfor
    <%block name="extra_top_menu_items"></%block>
    <!-- HEADER  -->
    <header>
        <%block name="page_header">
            <h1><a href="/" title="Torna a l'inici">${blog_title}</a></h1>
            <h1 id="mobile-main-title">
                <a href="/" title="Torna a l'inici">${blog_mobile_title}</a>
            </h1>
            % if link:
                    <p><a href='${link}'>${messages[lang]["Original site"]}</a></p>
            % endif
        </%block>
    </header>
    <%block name="content"></%block>
    <!-- POPUP BOX  -->
    <article id="contacte" class="reveal-modal">
        <div class="boxes">
            <div class="box">
                <h3>Contacte</h3>
                <p>Vols venir a les reunions, ens vols fer arribar algun dubte
                    o suggerència?<br />
                    Posa't en contacte amb nosaltres!
                </p>
                <div class="contactform">
                    <form id="emf-form" target="_self"
                        enctype="multipart/form-data" method="post"
                        action="http://www.emailmeform.com/builder/form/C828G1BM2119v3">
                        <!-- Nom -->
                        <input type="text" id="element_0" name="element_0"
                            maxlength="40" required="required"
                            placeholder="El teu nom" 
                            class="validate[required]"/>
                        <!-- E-mail o telèfon -->
                        <input type="text" id="element_1" name="element_1"
                            maxlength="40" required="required"
                            placeholder="El teu e-mail o telèfon"
                            class="validate[required]" />
                        <small>Per què ens poguem posar en contacte amb tu</small>
                        <!-- Assumpte -->
                        <input type="text" id="element_2" name="element_2"
                            maxlength="80" required="required"
                            placeholder="Assumpte"
                            class="validate[required]" />
                        <!-- Missatge -->
                        <textarea id="element_3" name="element_3"
                            required="required"
                            placeholder="Missatge"
                            class="validate[required] "></textarea>
                        <br/>
                        <!-- recaptcha -->
                        <script type="text/javascript">
                            var RecaptchaOptions = {
                                theme: 'custom',
                                custom_theme_widget: 'emf-recaptcha_widget'
                            };
                        </script>
                        <div id='emf-recaptcha_widget' style='display:none'>
                            <div id='recaptcha_image'></div>
                            <div id='recaptcha_controls'>
                                <a title='Get a new challenge'
                                    href="javascript:Recaptcha.reload()">
                                    <img src='//assets.emailmeform.com/images/recaptcha_refresh.png'
                                        alt='Get a new challenge'>
                                </a><!--
                                --><a title='Get an audio challenge'
                                    href="javascript:Recaptcha.switch_type('audio')"
                                    class='recaptcha_only_if_image'>
                                    <img src='//assets.emailmeform.com/images/recaptcha_audio.png'
                                        alt='Get an audio challenge'>
                                </a><!--
                                --><a title='Get a visual challenge'
                                    href="javascript:Recaptcha.switch_type('image')"
                                    class='recaptcha_only_if_audio'>
                                    <img src='//assets.emailmeform.com/images/recaptcha_image.png'
                                    alt='Get a visual challenge'>
                                </a><!--
                                --><a title='Help'
                                    href="javascript:Recaptcha.showhelp()">
                                    <img alt='Help'
                                        src='//assets.emailmeform.com/images/recaptcha_help.png'>
                                </a>
                            </div>
                            <img id='recaptcha_logo' style=''
                                src='https://www.google.com/recaptcha/api/img/clean/logo.png'>
                            <input type='text' id='recaptcha_response_field'
                                name='recaptcha_response_field'
                                placeholder='Escriu el text de la imatge'>
                        </div>
                        <script type="text/javascript"
                            src="https://www.google.com/recaptcha/api/challenge?k=6LchicQSAAAAAGksQmNaDZMw3aQITPqZEsX77lT9"></script>
                        <noscript>
                            <iframe src="https://www.google.com/recaptcha/api/noscript?k=6LchicQSAAAAAGksQmNaDZMw3aQITPqZEsX77lT9"
                                height="300" max-width="440" frameborder="0"></iframe>
                            <br/>
                            <textarea name="recaptcha_challenge_field" rows="3"
                                cols="40"></textarea>
                            <input type="hidden" name="recaptcha_response_field"
                                value="manual_challenge"/>
                        </noscript>
                        <!-- others -->
                        <input name="element_counts" value="4" type="hidden" />
                        <input name="embed" value="forms" type="hidden" />
                        <!-- button -->
                        <input type="submit" name="sendMessage"
                            id="sendMessage" class="button"
                            value="Envia el missatge"/>
                    </form>
                    <!--<script type="text/javascript">generate_css_for_emf_ad();</script>-->
                    <div id='emf_advertisement'>
                        Powered by
                        <span style="position: relative; padding-left: 3px; bottom: -5px;">
                            <img src="//assets.emailmeform.com/images/footer-logo.png" />
                        </span>
                        EMF
                        <a style="text-decoration:none;"
                            href="http://www.emailmeform.com/" target="_blank">
                            Online Survey Builder
                        </a>
                    </div>
                    <div>
                        <a style="line-height:20px;font-size:70%;text-decoration:none;"
                            href="http://www.emailmeform.com/report-abuse.html?http://www.emailmeform.com/builder/form/C828G1BM2119v3"
                            target="_blank">Report Abuse</a>
                    </div>
                </div>
            </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </article>
    <!-- POPUP SIDEBAR & MENU  -->
    <article id="menubox" class="reveal-modal">
        <div class="boxes">
            <!--
            <form id="searchform" class="searchbox">
                <input type="text" id="search" class="field searchtext"
                    placeholder="Keyword..."/>
                <input type="submit" class="button" name="submit" value="Go"/>
            </form>
            -->
            <div class="box">
                <h3>Menu</h3>
                <nav>
                    <ul>
                        <!--
                        if rel_link(permalink, url) == "#":
                            <li class="active"><a href="${url}">${text}</a>
                        endif:
                        -->
                        %for url, text, submenu in sidebar_links[lang]:
                            %if permalink and rel_link(isinstance(permalink, tuple) and permalink[0] or permalink, url) == "#":
                                <li class="active">
                            %else:
                                <li>
                            %endif
                                %if url:
                                    <a href="${url}">${text}</a>
                                %else:
                                    ${text}
                                %endif
                                %if len(submenu) > 0:
                                    <ul>
                                        %for suburl, subtext, submenu2 in submenu:
                                            <li><a href="${suburl}">${subtext}</a></li>
                                        %endfor
                                    </ul>
                                %endif
                            </li>
                        %endfor
                    </ul>
                </nav>
            </div>
            <div class="box">
                <ul class="social-icons">
                    <li><a href="https://www.facebook.com/dbellvitgekib"
                            title="Visita'ns al Facebook">
                        <img class="social-icon" src="/images/social-icons/social_facebook.png"
                                alt="Facebook"/>
                        </a></li>
                    <!--<li><a href="">
                        <img class="social-icon" src="/images/social-icons/social_twitter.png"
                                alt="Twitter"/></a></li>-->
                    <li>
                        <a href="mailto:info@diablesbellvitge.cat"
                            title="Envia'ns un e-mail">
                            <img class="social-icon"
                                src="/images/social-icons/correu.png"
                                alt="E-mail"/>
                        </a>
                    </li>
                    <!--
                    <li><a href=""><img class="social-icon" src="/images/social-icons/social_vimeo.png" alt="Vimeo"/></a></li>
                    <li><a href=""><img class="social-icon" src="/images/social-icons/social_you_tube.png" alt="YouTube"/></a></li>
                    -->
                </ul>
            </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </article>
    <!-- JS FILES  -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <!--<script src="/assets/js/jquery.js" type="text/javascript"></script>-->
    <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.imagesloaded.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.reveal.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.backstretch.min.js" type="text/javascript"></script>
    <!--<script src="/assets/js/jquery.tweet.js" type="text/javascript"></script>-->
    <%block name="extra_js">
    </%block>
</body>
</html>
