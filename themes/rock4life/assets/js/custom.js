﻿/* Opening Animations */
var section = $("section");
var sidemenu = $("nav ul li");
var mobile_max_size = 960;
$(document).ready(function () {
    "use strict";
    //section.eq(0).addClass("show");
    //$('#arrows').find('#prev').hide();
    //background();
    shorttitle();
    mobileheader();
});
function loading() {
    "use strict";
}

function shorttitle() {
    "use strict";
    if ($(window).width() < 1024) {
        $('header').find('h1').hide();
        $('header').find('#mobile-main-title').show();
    } else {
        $('header').find('h1').show();
        $('header').find('#mobile-main-title').hide();
    }
}

function mobileheader() {
    "use strict";
    if ($(window).width() < mobile_max_size) {
        $('.main').find('#mobile-header-image').show();
    } else {
        $('.main').find('#mobile-header-image').hide();
    }
}

/* Menu Icons Effect */
var menuitem = $(".menuitem");
menuitem.hover(

function (event) {
    "use strict";
    var $clickedElement = $(event.target);
    var $theid = $clickedElement.closest(menuitem);
    var $allListElements = menuitem.find('.info');
    $theid.find($allListElements).slideDown(100);
    $theid.find($allListElements).addClass('animated tada');

},

function (event) {
    "use strict";
    var $clickedElement = $(event.target);
    var $theid = $clickedElement.closest(menuitem);
    var $allListElements = menuitem.find('.info');
    $theid.find($allListElements).slideUp(100);
    $theid.find($allListElements).removeClass('animated tada');
});

/* Events Effect */
var grd = $(".archive .grid-3");
grd.hover(
function (event) {
    "use strict";
    var $clickedElement = $(event.target);
    var $theid = $clickedElement.closest(grd);
    var $allListElements = grd.find('h6');
    $theid.find($allListElements).fadeIn(100);

},

function (event) {
    "use strict";
    var $clickedElement = $(event.target);
    var $theid = $clickedElement.closest(grd);
    var $allListElements = grd.find('h6');
    console.log("function(event): clickedElement=", $clickedElement,
        ", theid=", $theid, ", allListElements=", $allListElements);
    $theid.find($allListElements).fadeOut(100);
});

$(window).resize(function () {
    "use strict";
    if (/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) === false) {
        location.reload();
    }
});


/////////////////* TABS */////////////////////////

$(document).ready(function () {
    "use strict";
    $("body").find("#tab-content div").hide();
    $("body").find("#tabs li:first").attr("id", "current");
    $("body").find("#tab-content div:first").fadeIn();

    $("body").find('#tabs a').on("click", function (e) {
        e.preventDefault();
        $("body").find("#tab-content div").hide();
        $("body").find("#tabs li").attr("id", "");
        $(this).parent().attr("id", "current");
        $('#' + $(this).attr('title')).fadeIn();
    });
});

/* Sidebar Menu */
sidemenu.on("click", function (event) {
    "use strict";
    var $clickedElement = $(event.target);
    var $theid = $clickedElement.closest(sidemenu);
    var $allListElements = $theid.find('ul li');
    if ($theid.hasClass('side-active')) {
        $allListElements.slideUp(100);
        $theid.find($allListElements).removeClass('animated fadeInLeft');
        $theid.removeClass('side-active');
    }
    else {
        $theid.addClass('side-active');
        $allListElements.slideDown(100);
        $theid.find($allListElements).addClass('animated fadeInLeft');
    }

});

/////////////////* LOADING ANIMATION ( DON'T REMOVE ! ) */////////////////////////

window.onload = loading;
