var homebody = $("#home");
var section = $("section");
var sidemenu = $("nav ul li");
var mobile_max_size = 960;
$(document).ready(function () {
    "use strict";
    section.eq(0).addClass("show");
    //$('#arrows').find('#prev').hide();
    background();
    //already added in custom.js
    //mobileheader();
    if ($(window).width() < mobile_max_size) {
        $('.content').find(".grids").slideUp(100);
    }

});

/* Article Opening Effect */
function openeffect() {
    "use strict";
    if ($(window).width() > 1024) {
        var clm = $(".show .column");
        console.log("openeffect(): $'.show .column'=", clm);
        clm.each(function (i) {
            var e = $(this);
            e.fadeTo(0, 0);
            setTimeout(function () {
                e.fadeTo(350, 1);
            }, i * 350);
        });
    }
}

/* Article Backgrounds */
function background() {
    "use strict";
    if ($(window).width() > mobile_max_size) {
        homebody.backstretch("/images/photos/logo.png");
        $('.main').find("#la-colla").backstretch("/images/photos/colla.jpg");
        $('.main').find("#activitats-propies").backstretch("/images/photos/activitats.jpg");
        $('.main').find("#participa").backstretch("/images/photos/participa.jpg");
        $('.main').find("#campanya-economica").backstretch("/images/photos/collabora.jpg");
    }
}

/* Clear Background Images */
function clear() {
    "use strict";
    $('.main').find(".backstretch img").remove();

}

if ($(window).width() > mobile_max_size) {
    /* Homepage Article Effects */
    var clmn = $('.column');
    var clmn_link = $('.column .page-title > a');
    clmn_link.click(function(event) {
        event.preventDefault();
    });
    /*var curtain = $('#curtain');*/
    clmn.on("click",

    function (event) {
        "use strict";
        /*curtain.stop().animate({
            'height': '0%'
        }, 10);*/
        var $clickedElement = $(event.target);
        var $theid = $clickedElement.closest(clmn);
        var $pagelink = $theid.find('.page-title > a');
        if ($theid.hasClass('active') === false) {
            $theid.addClass('active');

            clmn.each(function () {
                var e = $(this);
                if (e.hasClass('active')) {
                    $(this).stop().fadeOut(1000).delay(500).animate({
                        'width': '100%'
                    }, 10).fadeIn(500);
                } else {
                    $(this).addClass('animated fadeOutDown').delay(200).stop().fadeOut(1000).animate({
                        'width': '0%'
                    }, 10);
                }
            });
            if ($pagelink && $pagelink.length == 1 && $pagelink[0].href != '#') {
                /*$('.main').find('.active .desc').stop().delay(1000).animate({
                    'marginLeft': '25%'
                }, 1000);*/
                setTimeout(function() {
                    window.location = $pagelink[0].href;
                }, 1500);
                return;
            }
            $('.main').find('.active .desc').stop().delay(1000).animate({
                'marginLeft': '25%'
            }, 1000).delay(200).animate({
                'width': '75%',
                'height': '100%',
                'padding-top': '80px'
            }, 1000);
            homebody.find('#contactbutton').stop().animate({
                'marginRight': '55px'
            }, 100);
            homebody.find('#menu').stop().animate({
                'marginRight': '55px'
            }, 100);
            homebody.find('#galeria').stop().animate({
                'marginRight': '55px'
            }, 100);
            homebody.find('#agenda-interna').stop().animate({
                'marginRight': '55px'
            }, 100);
            homebody.find('#arrows').fadeOut();
            homebody.find('#close').fadeIn();
        }
    });

    homebody.find('#close').on("click",

    function () {
        "use strict";
        clmn.removeClass('animated fadeOutDown');
        /*curtain.stop().animate({
            'height': '100%'
        }, 500);*/
        $('.main').find('.active').stop().animate({
            'width': '25%'
        }, 1000);
        clmn.stop().delay(1000).animate({
            'width': '25%'
        }, 1000).delay(200).fadeIn("slow");
        clmn.find('.desc').stop().animate({
            scrollTop: 0,
            'height': '50px',
            'width': '100%',
            'padding-top': '0px'
        }, 800).animate({
            'marginLeft': '0'
        }, 1000);
        homebody.find('#close').fadeOut();
        homebody.find('#menu').stop().animate({
            'marginRight': '0px'
        }, 100);
        homebody.find('#contactbutton').stop().animate({
            'marginRight': '0px'
        }, 100);
        homebody.find('#galeria').stop().animate({
            'marginRight': '0px'
        }, 100);
        homebody.find('#agenda-interna').stop().animate({
            'marginRight': '0px'
        }, 100);
        homebody.find('#arrows').delay(2000).fadeIn();
        clmn.removeClass('active');
    });

    /* Paging Effects */
    var nextlink = $('#next');
    var prevlink = $('#prev');
    nextlink.on("click", function () {
        "use strict";
        $("body").find(".show").removeClass('show').next().addClass('show');
        clear();
        openeffect();
        background();

        if (section.first().hasClass('show')) {
            prevlink.fadeOut();
        } else {
            prevlink.delay(1500).fadeIn();
        }
        if (section.last().hasClass('show')) {
            nextlink.fadeOut();
        } else {
            nextlink.delay(1500).fadeIn();
        }

    });

    prevlink.on("click", function () {
        "use strict";
        $("body").find(".show").removeClass('show').prev().addClass('show');
        clear();
        openeffect();
        background();

        if (section.first().hasClass('show')) {
            prevlink.fadeOut();
        } else {
            prevlink.delay(1500).fadeIn();
        }
        if (section.last().hasClass('show')) {
            nextlink.fadeOut();
        } else {
            nextlink.delay(1500).fadeIn();
        }

    });

}
if ($(window).width() < mobile_max_size) {
    ////////////////* ACCORDION */////////////////////
    var dsc = $('.desc');
    var grds = $('.grids');
    dsc.on("click", function (event) {
        "use strict";
        var $clickedElement = $(event.target);
        var $theid = $clickedElement.closest(dsc);
        var open = ! $theid.find($(grds)).is(":visible");
        grds.slideUp(100);
        if (open) {
            var $allListElements = $(grds);
            $allListElements.addClass('active-grid');
            $theid.find($allListElements).slideDown(100);
            $(grds).removeClass('active-grid');
        }
    });
}
