# Copyright (c) Guillem Barba

# Permission is hereby granted, free of charge, to any
# person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the
# Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the
# Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice
# shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function, unicode_literals
from copy import copy
import glob
import pytz
import os.path

import logging
import pprint

from nikola.plugin_categories import Task
from nikola.post import Post
from nikola.utils import config_changed

from nikola import utils


class Home(Task):
    """Render the home."""

    name = "render_home"

    def scan_home_sections(self):
        """Create a nikola.post.Post for each page with source_path in
        'home_links' and put the list of posts in 'home_sections' of 'site'.
        It use as Template 'home_story.tmpl'"""
        if hasattr(self.site, 'home_sections') and self.site.home_sections:
            return
        #if self.site._home_scanned:
        #    return

        home_sections_template = 'home_story.tmpl'

        print("Scanning home sections", end='')
        tzinfo = None
        if self.site.config['TIMEZONE'] is not None:
            tzinfo = pytz.timezone(self.site.config['TIMEZONE'])
        full_list = []
        for lang in self.site.config['TRANSLATIONS'].keys():
            print(".", end='')
            for _, source_path, _, title in \
                    self.site.GLOBAL_CONTEXT['home_links'][lang]:
                if not source_path:
                    continue
                full_list.append(source_path)
                # Now let's look for things that are not in default_lang
                for lang in self.site.config['TRANSLATIONS'].keys():
                    lang_glob = source_path + "." + lang
                    translated_list = glob.glob(lang_glob)
                    for fname in translated_list:
                        orig_name = os.path.splitext(fname)[0]
                        if orig_name in full_list:
                            continue
                        full_list.append(orig_name)
        post_by_source = {}
        for base_path in full_list:
            post = Post(
                base_path,
                self.site.config['CACHE_FOLDER'],
                "",
                False,
                self.site.config['TRANSLATIONS'],
                self.site.config['DEFAULT_LANG'],
                self.site.config['BASE_URL'],
                self.site.MESSAGES,
                home_sections_template,
                tzinfo=tzinfo,
            )
            post_by_source[base_path] = post
        self.site.home_sections = post_by_source
        #self.site._home_scanned = True
        print("done!")

    def gen_tasks(self):
        self.scan_home_sections()
        logging.debug("post_by_source: %s" % self.site.home_sections)
        post_by_source = self.site.home_sections

        kw = {
            "translations": self.site.config["TRANSLATIONS"],
            "filters": self.site.config["FILTERS"],
            "hide_untranslated_posts":
                self.site.config['HIDE_UNTRANSLATED_POSTS'],
        }

        for lang in kw["translations"]:
            # (Only) compile the sources of home sections
            for task in self.home_section_compiler(lang, post_by_source, kw):
                task['uptodate'] = [config_changed({
                    1: task['uptodate'][0].config,
                    2: kw})]
                task['basename'] = self.name
                #task['task_dep'] = ['render_posts', 'render_pages']
                logging.debug("HOME SECTION RENDER: %s\n"
                    % pprint.pformat(task))
                yield task
            # Render the home supplying the home_sections posts list
            for task in self.home_renderer(lang, post_by_source,
                    kw['filters']):
                task['uptodate'] = [config_changed({
                    1: task['uptodate'][0].config,
                    2: kw})]
                task['basename'] = self.name
                task['task_dep'] = ['render_posts', 'render_pages']
                flag = True
                logging.debug("HOME task: %s\n" % pprint.pformat(task))
                yield task
        if flag is False:  # No page rendered, yield a dummy task
            yield {
                'basename': self.name,
                'name': 'None',
                'uptodate': [True],
                'actions': [],
            }

    def home_section_compiler(self, lang, post_by_source, kw):
        deps_dict = copy(kw)
        for post in post_by_source.values():
            source = post.source_path
            dest = post.base_path
            if (not post.is_translation_available(lang) and
                    kw["hide_untranslated_posts"]):
                continue
            else:
                source = post.translated_source_path(lang)
                if lang != post.default_lang:
                    dest = dest + '.' + lang
            task = {
                'name': dest,
                'file_dep': post.fragment_deps(lang),
                'targets': [dest],
                'actions': [
                    (self.site.get_compiler(post.source_path),
                        [source, dest]),
                ],
                'clean': True,
                'uptodate': [utils.config_changed(deps_dict)],
            }
            yield task

    def home_renderer(self, lang, post_by_source, filters):
        """Render post fragments to final HTML pages."""
        context = {}
        deps = self.site.template_system.template_deps('home.tmpl')
        #for post in post_by_source.values():
        #    deps += post.deps(lang)
        context['lang'] = lang
        context["posts"] = post_by_source
        context["title"] = self.site.config['BLOG_TITLE']
        context["description"] = self.site.config['BLOG_DESCRIPTION']
        context['permalink'] = self.site.config['BASE_URL'],
        context['page_list'] = self.site.pages
        context['enable_comments'] = False
        output_name = os.path.join(self.site.config['OUTPUT_FOLDER'],
            'index.html')
        deps_dict = copy(context)
        deps_dict['OUTPUT_FOLDER'] = self.site.config['OUTPUT_FOLDER']
        deps_dict['TRANSLATIONS'] = self.site.config['TRANSLATIONS']
        deps_dict['posts'] = [(p.meta[lang]['title'], p.permalink(lang))
                for p in post_by_source.values()]
        deps_dict['global'] = self.site.GLOBAL_CONTEXT
        deps_dict['comments'] = False
        logging.debug("home_render(): output_name=%s, type(self.site)=%s"
            % (output_name, type(self.site)))
        task = {
            'name': os.path.normpath(output_name),
            'file_dep': deps,
            'targets': [output_name],
            'actions': [(self.site.render_template, ['home.tmpl', output_name,
                        context])],
            'clean': True,
            'uptodate': [config_changed(deps_dict)],
        }
        yield utils.apply_filters(task, filters)
