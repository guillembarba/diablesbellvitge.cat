.. link: 
.. description: 
.. tags: 
.. date: 2013/04/22 09:42:16
.. title: Activitats pròpies
.. slug: activitats-propies

.. raw:: html

    <div class="grid-4">
        <img class="cover" src="/images/photos/post.jpg" alt=""/>
    </div>
    <div class="grid-8">
        <h3>Nit Invernal</h3>
        <hr/>
        <p>
            Matinada, correfoc i esectacle de plaça per inaugurar la <strong>Festa major</strong>.
        </p>
        <p>
            <strong>On:</strong> De l'Ermita de Bellvitge a la plaça del Bernat Metge<br/>
            <strong>Quan:</strong> 7 de setembre de 2013 (primer dissabte de festes), 20:30h<br/>
            <strong>Colles convidades:</strong> Bracadabra, Fills de la flama, Florida, Hospitalet, Kabra, La vella de Gràcia, Sant Joan Despí, Sant Vicens dels Horts, Skamot, Vilanova<br/>
        </p>
    </div>
    <hr/>

    <div class="grid-4">
        <a href="/nit-jove/"><img class="cover" src="/images/photos/post.jpg" alt=""/></a>
    </div>
    <div class="grid-8">
        <h3><a href="/nit-jove/">Nit Jove</a></h3>
        <hr/>
        <p>
            Concert gratuit dins de la Festa Major de Bellvitge, amb servei de barra i entrepans.
        </p>
        <p>
            <strong>On:</strong> Parc nou de Bellvitge<br/>
            <strong>Quan:</strong> 14 de setembre de 2013 (segon dissabte de festes), 22h<br/>
            <strong>Grups:</strong> Els catarres i Rumba sin rumbo (taloners)<br/>
        </p>
    </div>

    <div class="grid-4">
        <img class="cover" src="/images/photos/post.jpg" alt=""/>
    </div>
    <div class="grid-8">
        <h3>Karnaval</h3>
        <hr/>
        <p>
            Construcció del Carnestoltes, participació a la rua del barri i espectacle del Judici del
            carnestoles.
        </p>
        <p>
            <strong>On (espectacle):</strong> Plaça del mercat vell de Bellvitge<br/>
            <strong>Quan (espectacle):</strong> Diumenge de carnaval, 20h (a confirmar)<br/>
            <strong>Motiu:</strong> Pendent<br/>
        </p>
    </div>

