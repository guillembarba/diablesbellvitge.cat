.. link: 
.. description: 
.. tags: 
.. date: 2013/04/28 18:01:55
.. title: Karnaval 2010
.. slug: karnaval/karnaval-2010

.. contents:: Continguts
   :backlinks: top
   :class: cover-info


-----------
Introducció
-----------

Som la Colla de Diables i Diablesses de Bellvitge i amb motiu del pròxim 30è aniversari volem organitzar diferents actes i activitats per celebrar-lo. Amb aquests també pretenem donar a conèixer la Colla a la gent jove del barri i a qui encara no ens conegui.

Per tal de portar a terme els projectes que us presentem a continuació, necessitem la col·laboració de les administracions públiques, ja que amb les diferents campanyes econòmiques que duem a terme, no arribem més que per cobrir les despeses anuals de la Colla; i tampoc podem assumir les activitats tal i com creiem que s'haurien d'organitzar per dur a la pràctica. 
Iniciarem el calendari amb la participació a la tradicional Rua del Carnaval i amb la Crema del Rei Carnestoltes del barri de Bellvitge.

Amb el propòsit de recuperar activitats perdudes, a l’Abril, organitzarem la Diada de Diables dirigida al barri i amb la participació de les diferents Colles convidades a aquesta amb tallers pel matí dirigits als més petits (organitzat conjuntament amb la colla de Diables del Club d’Esplai Bellvitge), la cursa de bèsties i una tabalada per la tarda i un concert de grups novells per la nit.

L'estiu el començarem amb un torneig de futbol dedicat a potenciar la germanor entre les diferents colles assistents i passar-ho bé gaudint de l'esport a l'aire lliure.
Durant aquest any inaugurarem una exposició en la que es recorrin els 30 anys de la Colla exposant fotografies, vestits i masses històrics de la Colla, tot acompanyat de cartells informatius. A més a més, s’estrenarà un documental en el que s’expliquin els 30 anys de la Colla a través d’algunes de les persones que han passat per la Colla des de l’any 1983 fins a l’actualitat. 

Tornant de l'estiu arriben les Festes de Bellvitge i per tant el nostre moment. És l'any de la 30ª Nit Infernal i farem que no s'oblidi en el pròxims anys.   
Us animem a que participeu amb nosaltres en aquest projecte i en les activitats proposades. 

Salut i foc!!


.. figure:: /images/logo-tro.jpg
   :target: https://www.facebook.com/tro.hospitalet
   :class: thumbnail
   :alt: Logo de Tro: Coordinadora d'entitats de foc de l'Hospitalet


