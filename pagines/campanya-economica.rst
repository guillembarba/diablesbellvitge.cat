.. link: 
.. description: 
.. tags: 
.. date: 2013/05/09 20:02:48
.. title: Campanya econòmica
.. slug: campanya-economica/index

..        <img src="/images/banner-merchandasing.png"
..                alt="Merchandising: la botiga del KIB"/>

.. raw:: html

    <ul class='kwicks kwicks-horizontal'>
        <li id='panel-1'>
            <h3><a href="#content-1">Encenedors</a></h3>
            <img src="/images/photos/encenedors.jpg"/>
        </li>
        <li id='panel-2'>
            <h3><a href="#content-2">Mocadors</a></h3>
            <img src="/images/photos/mocador.jpg"/>
        </li>
        <li id='panel-3'>
            <h3><a href="#content-3">Semarretes</a></h3>
            <img src="/images/photos/semarretes-dessuadores.jpg"/>
        </li>
        <li id='panel-4'>
            <h3><a href="#content-3">Dessuadores</a></h3>
            <img src="/images/photos/semarretes-dessuadores.jpg"/>
        </li>
    </ul>

Des de la sortida de la nova normativa europea de la "Llei del Foc" les colles de diables
hem de comptar amb una nova assegurança i en cada espectacle o correfoc que organitzem s'ha de
tramitar un permís que costa bastants diners. En un temps en que les administracions no tenen
molts recursos, som les pròpies colles les que hem d'intentar mantenir-nos en peu, tot i les
dificultats que ens trobem.

La colla de Diables de Bellvitge (KIB) som una entitat sense afany de lucre, que intentem mantenir
viva la tradició del foc al nostre barri. És per això, que et demanem la teva ajuda, i com pots fer-ho?

Doncs si el món del foc t'agrada, no dubtis en venir un dia al local i veure i viure el funcionament
d'una colla, **sempre fa falta gent !!!**

Però si el foc et fa respecte, també tens altres formes de col·laborar. A la part inferior podràs veure
productes de la colla, que rebràs a canvi d'una aportació. 

Des d'ara volem donar-te les gràcies per fer possible que Bellvitge conservi viva la cultura i
la tradició del foc mitjançant la Colla de Diables i Diablesses de Bellvitge (KIB).
   
En motiu del 30è aniversari que aquest any 2013 celebrem, iniciem una campanya
econòmica **per soportar les activitats extres que realitzem i per les quals no
tenim subvernció**, a més d'ajudar-nos a cobrir altres despeses com
l'assegurança de la colla (necessària per poder organitzar `les nostres
activitats`_.

.. _les nostres activitats: /#albums

No totes les activitats que organitzem estan suportades completament pel
pressupost... alguns perquè són activitats extraordinàries que no estan
contemplades a les subvencions o assignacions habituals, i altres cops perquè
volem fer un espectacle superior al que podriem fer amb els diners que ens
donen.

Enguany volem celebrar l'aniversari de la colla i ho farem fent més i millors
activitats... però no aconseguirem tots els diners que necessitem de fons
públics. **Ens ajudes?**

.. raw:: html

    <div class="grid-4" id="content-1">
        <img class="cover" src="/images/photos/encenedors.jpg" alt=""/>
    </div>
    <div class="grid-8">
        <h3>Encenedors<span class="date">1,5 €</span></a></h3>
        <hr/>
        <p>
            Encenedors recargables de mida petita amb el logo de *la cartxofa*
            i l'any de fundació de la colla.
        </p>
    </div>
    <hr/>

    <div class="grid-4" id="content-2">
        <img class="cover" src="/images/photos/mocador.jpg" alt=""/>
    </div>
    <div class="grid-8">
        <h3>Macadors <span class="date">3 €</span></a></h3>
        <hr/>
        <p>
            Imprescindible per anar de correfoc.
        </p>
        <p>
            Mocador de cotó amb el logotip de la colla
            amb el text *Diables i Diablesses de Bellvitge*.
        </p>
    </div>
    <hr/>

    <div class="grid-4">
        <img class="cover" src="/images/photos/semarretes-dessuadores.jpg" alt=""/>
    </div>
    <div class="grid-8" id="content-3">
        <h3>Semarretes i dessuadores <span class="date">de 3 a 12 €</span></a></h3>
        <hr/>
        <p>
            T'oferim un model estàndard de cada cosa... però ens pots portar
            la teva pròpia semarreta o dessuadora i et cobrem només la impressió.
        </p>
        <h4>Posem nosaltres la roba</h4>
        <p>
            Ens dius la talla i el color que vols, te la fem i quedem per
            donar-te-la i que fagis la donació.
        </p>
        <p>
            <strong>Semarreta cotó <em>Decathlon</em> (1 cara): </strong> 6 €<br/>
            <strong>Dessuadora cotó <em>Decathlon</em> (1 cara): </strong> 12 €<br/>
        </p>
        <h4>Ens dones tu la roba</h4>
        <p>
            Porta'ns la peça de roba que vulguis personalitzar i nosaltres et
            fem la impressió.
        </p>
        <ul>
            <li>Impressió 1 cara 1 color: 3 €</li>
            <li>Alguna altra opció? 5 €</li>
        </ul>
    </div>
    <hr/>

