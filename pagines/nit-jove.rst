.. link: 
.. description: 
.. tags: archive
.. date: 2013/04/25 23:12:00
.. title: XV Nit Jove
.. slug: nit-jove/index

.. figure:: /images/cartell-xv-nit-jove.jpg
   :class: thumbnail
   :alt: Cartell de la XV Nit Jove

.. raw:: html

    <ul class="simple">
        <li><strong>Grup principal</strong>: <a href="http://www.elscatarres.cat/agenda/14-09-13-l-hospitalet-de-llobregat#c20" target="_blank" title="Mira l'anunci a la seva pàgina web">Els catarres</a></li>
        <li><strong>Taloners</strong>: <a href="http://www.rumbasinrumbo.com" target="_blank">Rumba sin rumbo</a></li>
        <li><strong>Lloc</strong>: <a href="https://maps.google.com/maps?q=41.347973,2.110919&hl=ca&sll=41.348013,2.110823&sspn=0.003234,0.008256&t=h&z=17" title="Localitza el parc" target="_blank">Parc nou de Bellvitge</a></li>
        <li><strong>Quan</strong>: Dissabte 14 de setembre de 2013, 22h</li>
    </ul>


=========
Anteriors
=========

.. raw:: html

    <div class="grid-3">
        <img class="event-img" src="/images/photos/post.jpg" alt=""/>
        <!--<a href="single-post.html">-->
        <h6>
            <span class="event-title">XIV</span> Nit Jove<br/>
            <span class="event-info">The Pepper Pots<br/>
                Surfin Burritos</span>
        </h6>
        <!--</a>-->
    </div>
    <div class="grid-3">
        <img class="event-img" src="/images/photos/post.jpg" alt=""/>
        <h6>
            <span class="event-title">XIII</span> Nit Jove<br/>
            <span class="event-info">Pepito<br/>De los palotes</span>
        </h6>
    </div>
    <div class="grid-3">
        <img class="event-img" src="/images/photos/post.jpg" alt=""/>
        <h6>
            <span class="event-title">XII</span> Nit Jove<br/>
            <span class="event-info">Pepito<br/>De los palotes</span>
        </h6>
    </div>
    <div class="grid-3">
        <img class="event-img" src="/images/photos/post.jpg" alt=""/>
        <h6>
            <span class="event-title">XI</span> Nit Jove<br/>
            <span class="event-info">Pepito<br/>De los palotes</span>
        </h6>
    </div>

    <div class="grid-3">
        <img class="event-img" src="/images/photos/post.jpg" alt=""/>
        <h6>
            <span class="event-title">X</span> Nit Jove<br/>
            <span class="event-info">Pepito<br/>De los palotes</span>
        </h6>
    </div>
    <div class="grid-3">
        <img class="event-img" src="/images/photos/post.jpg" alt=""/>
        <h6>
            <span class="event-title">IX</span> Nit Jove<br/>
            <span class="event-info">Pepito<br/>De los palotes</span>
        </h6>
    </div>

