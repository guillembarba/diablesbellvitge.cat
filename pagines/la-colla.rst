.. description: La descripció crec que no s'utilitza
.. tags: 
.. date: 2013/02/05 22:50:52
.. title: La colla?
.. slug: la-colla

.. raw:: html

    <div class="grid-12">

-----------
Introducció
-----------

Som la Colla de Diables i Diablesses de Bellvitge i amb motiu del pròxim 30è aniversari volem organitzar diferents actes i activitats per celebrar-lo. Amb aquests també pretenem donar a conèixer la Colla a la gent jove del barri i a qui encara no ens conegui.

Per tal de portar a terme els projectes que us presentem a continuació, necessitem la col·laboració de les administracions públiques, ja que amb les diferents campanyes econòmiques que duem a terme, no arribem més que per cobrir les despeses anuals de la Colla; i tampoc podem assumir les activitats tal i com creiem que s'haurien d'organitzar per dur a la pràctica. 
Iniciarem el calendari amb la participació a la tradicional Rua del Carnaval i amb la Crema del Rei Carnestoltes del barri de Bellvitge.

Amb el propòsit de recuperar activitats perdudes, a l’Abril, organitzarem la Diada de Diables dirigida al barri i amb la participació de les diferents Colles convidades a aquesta amb tallers pel matí dirigits als més petits (organitzat conjuntament amb la colla de Diables del Club d’Esplai Bellvitge), la cursa de bèsties i una tabalada per la tarda i un concert de grups novells per la nit.

L'estiu el començarem amb un torneig de futbol dedicat a potenciar la germanor entre les diferents colles assistents i passar-ho bé gaudint de l'esport a l'aire lliure.
Durant aquest any inaugurarem una exposició en la que es recorrin els 30 anys de la Colla exposant fotografies, vestits i masses històrics de la Colla, tot acompanyat de cartells informatius. A més a més, s’estrenarà un documental en el que s’expliquin els 30 anys de la Colla a través d’algunes de les persones que han passat per la Colla des de l’any 1983 fins a l’actualitat. 

Tornant de l'estiu arriben les Festes de Bellvitge i per tant el nostre moment. És l'any de la 30ª Nit Infernal i farem que no s'oblidi en el pròxims anys.   
Us animem a que participeu amb nosaltres en aquest projecte i en les activitats proposades. 

Salut i foc!!


---------------------------------------------------------
Història de la Colla de Diables i Diablesses de Bellvitge
---------------------------------------------------------

L' inici de la colla es remunta al 1983 quan un grup de joves del Club Infantil i Juvenil de Bellvitge, amb l'acompanyament d'alguns monitors, que havien construït el Drac “la Gallina” tres anys abans, van decidir formar el Grup de Diables de Bellvitge per acompanyar aquesta bèstia a les seves sortides, sent la primera la del Correfoc de les festes majors de La Mercè (Barcelona) del mateix any.
La primera participació de la colla en la Crema del Carnestoltes del barri fou l'any següent, al 1984, i aquell mateix any ja participaren en més de 15 sortides a l'Hospitalet i Barcelona. La colla anava creixent en número i van haver de fer vestits nous.

El 1985 el grup s'independitza del CIJB i, davant la falta de local, varen anar peregrinant per diferents cases de membres de la colla fent les reunions setmanals i organitzant els diferents actes de la colla. Aquell any van estendre les seves sortides al Baix Llobregat i altres indrets de Catalunya. Aquest també va ser un any important ja que organitzaren la 1ª Nit Infernal en el marc de les Festes Majors de Bellvitge. Aquest acte és el més important de l'any per nosaltres i s'ha mantingut any rere any. Apadrinaren als Diables de Sant Joan d'Espí amb la qual es va establir un fort vincle que avui dia encara es manté. També es participa al primer “Pratifoc” (El Prat del Llobregat) i a la “V Trobada de diables de Catalunya” al Vendrell.

La colla segueix creixent tant en membres i sortides, així com en la qualitat del grup de percussió que l'acompanya.
El 1987 la Crema del Carnestoltes comença a tenir forma, aconseguint que assisteixi molt de públic. Seguim participant a les Trobades de Diables de Catalunya com cada any.
L'any següent es tornen a fer nous vestits i al 1989 va ser el torn de les maces, al carro i els instruments de música. Aquest mateix any és la 5ª Nit Infernal on es fa el primer rappel, la baixada a l' Infern, i torna a haver-hi molt de públic.

Després de 6 anys d'activitat intensa es redacten els primers estatuts de la colla el 1989, però no es presenten a la Generalitat fins el març del 1990, i quedàrem inscrits al registre el setembre d'aquell any amb el nom de Colla de Diables de Bellvitge.

Els propers anys vam continuar participant en les diferents festes populars de les comarques d’arreu de Catalunya. Però la Colla necessitava nous reptes, i es decidí fer un taller de màscares per complementar els vestits. Finalment van veure que amb la mateixa tècnica i amb materials similars es podria crear una bèstia, així que creàrem La Rata de Bellvitge. La construcció s’inicià al 1994 amb al cap. I és el 1995 quant tinguérem tota La Rata construïda i pintada. 
A l’any 1996, junt a altres colles de l'Hospitalet, creàrem l'associació TRO– Entitats de Foc de l'Hospitalet en la què continuem encara.

El 1997 els diables vam bufar 15 espelmes, aniversari que el férem coincidir amb l’organització del primer “Txiringuito” alternatiu de les festes de Bellvitge,  projecte que es mantingué durant els dos anys següents, i que es traspassà a una associació creada per portar a terme aquell “Txiringuito” alternatiu.

Durant els anys següents La Colla patí una baixada de personal progressiva i per tant de les activitats consolidades,  ja que la gent no es veia amb cor de respondre a elles, com ho havia fet fins aquell moment. 
No fou fins al 2003 que la Colla va començar poc a poc a reactivar-se, reprenent tan sortides com activitats que fins aleshores, s’havien deixa’t de banda.

En aquests darrers anys La Colla s’ha anat ampliant i consolidant amb l’ integració de nous membres  i s’ha donat especial èmfasi a l’elaboració dels espectacles propis i les sortides al barri.

A l’any 2008 la Colla bufa les espelmes del seu 25è aniversari. La Colla organitza diferents activitats, mantenint els ja tradicionals actes de la Nit Infernal, Carnaval i el concert de la Nit Jove; aquestes activitats van estar orientades essencialment a donar a conèixer la Colla al barri i a la ciutat, al igual que la cultura del foc i de la tradició catalana. Algunes d’aquestes activitats varen estar enfocades per tal de crear un espai de retrobament dels antics membres de la Colla.

Actualment estem treballant en els projectes d’aquest any, com donar a conèixer la Colla i fer partícips  la gent jove de Bellvitge, crear una nova bèstia més petita per poder dur als diferents correfocs i també preparant el trentè aniversari on esperem complir amb totes les expectatives plantejades.


---
Tro
---

.. figure:: /images/logo-tro.jpg
   :target: https://www.facebook.com/tro.hospitalet
   :alt: Logo de Tro: Coordinadora d'entitats de foc de l'Hospitalet


TRO, creada l’any 1996 per iniciativa d’un conjunt de colles de diables de la ciutat, neix com a Coordinadora de les Entitats de Foc de L’Hospitalet. Actualment aglutina 15 colles de diables (8 d’infantils i 7 d’adults) i  7 colles de bestiari.

Figura entre els objectius de TRO “col·laborar en la recuperació i el desenvolupament del patrimoni tradicional, cultural, popular, folklòric i etnològic del nostre país”; així com “promoure la participació a les activitats culturals de dinamització festiva i comunitària a la ciutat de L’Hospitalet de Llobregat i als seus barris”.

.. raw:: html

    </div>
