.. title: Karnaval 2013
.. slug: karnaval/karnaval-2013
.. date: 2013/02/02 22:43:00
.. tags: 
.. link: 
.. description:

=============
Karnaval 2013
=============

Esquema
=======

#. Inici

   * **Música**: `Circus Theme music`_
   * Carnestoltes al centre presentat amb un focus (llum a un pal)
   * **Presentador**: Benvinguda al circ

       Bona nit Bellvitjans i Bellvitjanes! per fi el **Circ ibèric** ha
       arribat al vostre barri. No tenim trapecistes, ni elefants, ni dones
       barbudes ni homes bala. Prepareu-vos, perquè el esteu a punt de veure i
       escoltar és el maravellós món fantàstic del **Circ ibèric**.

   * 10 fogueres enceces electricament

#. Alegria

   * **Música**: `Alegria (Circ du Soleil)`_ o `Drooling banjos (The toy
     dolls)`_
   * Bicicleta + Patums
   * **Presentador**: Descripció del que passa en plan *positiu* (és nomal, no
     passa res...)

       En aquest circ tenim transport sostenible, subvencions a les energies
       renovables, *Plans E* per incentivar la construcció... què sempre ens ha
       fet falta.

       Tenim un polític que no ha robat i la policia que mai ha pegat
       (**Audio**: tros de la declaració del Felip Puig negant les boles de
       goma). Tenim un banquer molt honrat (**Audio**: Botin parlant sobre
       comptes suissos de la seva familia o cas Bankia) y a la familia real
       donant exemple al pais (**Audio**: el rei demanant disculpes).

   * **Música**: `Ball de rojos (La gossa sorda)`_
   * **Petada** normal

#. Descoberta mentida

   * **Música**: `Parnaso Ska (Potato)`_
   * Malabares
   * **Presentador**: Presentació del que passa ja més cabrejats.

       No us equivoqueu amics i amigues... el que aquí tenim i ens sobra són
       pallassos i macs. Tratges per fer patxoca amb sobres plens a les
       butxaques (**Audio**: Soraya I love you). Seus de partis embargades
       (**Audio**: algo sobre Cas palau i CiU) i concessions incentivades.

#. Machaque

   * **Presentador**: *¡que se jodan!*

      Privatitzem per ser eficients... i per donar feina a polítics retirats
      (**Audio**: algo sobre Aznar a Endesa, Rato a Bankia o Telefónica, De la
      Vega a Telefonica). I als currantes que ens carreguem... els hi diem
      *¡Qué se jodan!*

   * **Música**: `El circo ibérico (SKA-P)`_
   * **Petada** color + roncadora

#. Fin de fiesta (encesa foguera petada final)

   * **Música**: `Hey Boy Hey Girl (Chemical Brothers)`_
   * **Presentador**: Personajes a la hoguera a petición del público

      I què fem amb tot això ciutadans i ciutadanes? Ens ho mirem des de la
      tele. Cridem pel Twitter? Ens idignem pel Facebook? Sortim al carrer
      només per *La Roja* i la *Champions*?

      Jo us dic que no. Que cridem. Que ens manifestem. Que cremem.

      Busquem... i cremem.

      Investiguem... i cremem.

      Denunciem... i cremem.

      Ens informem... i cremem.

   * **Música**: Continua `Hey Boy Hey Girl (Chemical Brothers)`_ fent
     introducció i canvia a `Sehnsucht (Rammstein)`_
   * Encendida foguera
   * **Petada** final
   * **Presentador**: Comiat

      Fins aquí el **Circ Ibèric**. El carnaval 2013 ha acabat. Per 30a vegada
      els diables han cremat. I ho seguirem fent. Visca el foc, visca el vi i
      visca la mare que ens va parir.

   * Carpa en cascada
   * Traca valenciada

.. _Circus Theme music: http://www.youtube.com/watch?v=1D5Sa2Yq-2g
.. _Alegria (Circ du Soleil): http://www.youtube.com/watch?v=smkiSJf2cHE
.. _Drooling banjos (The toy dolls): http://www.youtube.com/watch?v=Sjs_VZRfAk0
.. _Ball de rojos (La gossa sorda): http://grooveshark.com/#!/s/Ball+De+Rojos/3cL66m?src=5
.. _Parnaso Ska (Potato): Ball de rojos (La gossa sorda)
.. _El circo ibérico (SKA-P): http://www.youtube.com/watch?v=wH8ZHZ9VIv0
.. _Hey Boy Hey Girl (Chemical Brothers): http://grooveshark.com/#!/s/Hey+Boy+Hey+Girl+Dj+Sundance+Mix/4gdESj?src=5
.. _Sehnsucht (Rammstein): http://www.youtube.com/watch?v=FCwHs81CNd8

